## Instalação API

* Clone o repositório no local da sua preferência
* Entre na pasta da api
* Faça a instalação dos pacotes necessários para rodar o servidor: npm i
* Crie um banco de dados MySQL/MariaDB chamado "brainnyteste"
* Configure os seguintes campos no arquivo ormconfig.json: 
    * type: Sistema de gerenciamento de banco de dados, default: "mysql"
    * username: Nome do usuário
    * password: Senha para acesso ao sistema
* Execute o servidor: npm start

O servidor cria um usuário root imediatamente a fim de testes e acesso imediato ao sistema. 

## Instalação do cliente Web

* Clone o repositório no local da sua preferência
* Entre na pasta web
* Faça a instalação dos pacotes necessários para rodar o cliente web: npm i
* Execute o cliente: npm start
* Endereço: localhost:3200/login
* Usuário de administrador:
    * email: root
    * senha: root

## Tecnologias e bibliotecas utilizadas
### Api

* Node.js com Express
* Typescript
* Apollo Server
* GraphQL
* JsonWebToken
* TypeORM

### Cliente Web

* React
* Apollo Client
* ReactStrap
* Moment
* Axios

## Dificuldades
### Api

* Estruturar o projeto com o Apollo Server
* Comunicação GraphQL
* Subscription

 Tive uma dificuldade para iniciar o projeto, estava seguindo a documentação normal do Apollo
 Server, quando deveria estar seguindo a documentação específica de Express, demorei um pouco
 para alcançar a estrutura do projeto, pois, vi que há diversas formas de iniciar o projeto. Ainda mais
 que baixei alguns exemplos do github para me orientar e ler como é feito, porém acabaram
 dificultando um pouco e causando mais dúvidas. No fim, a documentação do Apollo Server Express do site
 oficial ajudou bastante.
 Tive dificuldade na comunicação entre cliente e servidor, estava errando em algumas nomenclaturas,
 então, iniciei a utilizar o Postman, assim pude identificar melhor se o problema era no cliente ou servidor.
 Devido ao tempo e a pressão de retomar um projeto da faculdade em andamento, acabei optando por não
 dar foco em Subscription, cheguei a ler um pouco da documentação mas achei que poderia apertar o tempo para
 finalizar outras coisas deste projeto.

### Cliente Web

* Transição entre rotas
* Tratamento de erros do servidor
* Estilização

 Ao longo do projeto tive problema na mudança das rotas entre login e a main page,
 acabei resolvendo o problema na gambiarra trocando a url local da página e dando
 refresh.
 A falta de tratamento de erros no servidor acabou causando paradas inesperadas na página,
 a maioria foi corrigida, porém gostaria de ter trabalhado melhor nos retornos de status.
 Vejo a necessidade de estudar melhor css e de como lidar com os componentes, não fiquei
 satisfeito com o visual final.
 
## Ficou Faltando

* Admin ver a lista de usuários por sistema de Subscription atualizando em tempo real
* Desenvolvimento de testes automatizados
* Retorno adequado dos status na api
* Tratamento adequado dos status vindos da api no cliente
* Retorno adequado de entidades
* Gostaria de ter colocado mais features para o admin
* Gostaria de ter melhorado as formas de alertar o usuário sobre erros de conexão
 