import React, { Component, Fragment } from 'react'
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import { Container, Spinner } from 'reactstrap'

import { UserMenu } from '../components/UserMenu'
import { AdminMenu } from '../components/AdminMenu'

import { moment } from 'moment'

const GET_USER = gql`
    query {
        user {
            admin
        }
    }
`;

export default class Main extends Component {
    constructor(props) {
        super(props)

        if (!localStorage.getItem('token')) {
            window.history.pushState('', 'Login', '/login');
            window.location.reload()
        }
    }

    componentDidMount = async () => {

    }

    render() {
        return (
            <Container>
                <Fragment>
                    <Query query={GET_USER}>
                        {({ loading, error, data }) => {
                            if (loading) return <Spinner color="info" />
                            if (error) console.log(error)

                            try {
                                if (!data.user.admin) return <UserMenu />
                                else if (data.user.admin) return <AdminMenu />
                            } catch {
                                setTimeout(function () { window.location.reload(); }, 3000);
                                return (<h4>Servidor não respondeu, tentando novamente em 3 segundos...</h4>)
                            }
                        }}
                    </Query>
                </Fragment>
            </Container>
        )
    }
}
