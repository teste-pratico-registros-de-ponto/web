import React, { Component } from 'react'
import axios from 'axios'
import { Button, Form, FormGroup, Label, Input, Spinner } from 'reactstrap'

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mail: '',
            password: '',
            loading: false,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        localStorage.removeItem('token')
    }

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value })
    }

    componentDidMount = () => {

    }

    clearFields = () => {
        this.setState({ mail: '' })
        this.setState({ password: '' })
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({ loading: true })
        axios({
            method: 'post',
            url: 'http://localhost:3200/auth/login',
            data: {
                mail: this.state.mail,
                password: this.state.password,
            },
            validateStatus: (status) => {
                return true;
            },
        }).catch(error => {

        }).then(async response => {
            this.setState({ loading: false })
            try {
                const status = response.request.status
                if (status === 200) {
                    const token = response.data.token;
                    localStorage.setItem("token", token);
                    this.props.history.push("/")
                    window.location.reload()
                } else if (status === 401) {
                    alert("Erro de credenciais")
                    this.clearFields()
                } else {
                    alert("Erro de credenciais ou de servidor")
                    this.clearFields()
                }
            } catch (err) {
                console.log(err)
            }
        });
    }

    render() {
        return (
            <div className="container col-sm-4 align-center mt-4">
                <div className="text-center mb-4">
                    <img src={require("../icons/logo.png")} alt="logo" style={{ width: "150px" }} />
                </div>
                <Form onSubmit={this.handleSubmit}>
                    <FormGroup>
                        <Label for="mail">Email</Label>
                        <Input type="text" name="mail" id="mail" value={this.state.mail} onChange={this.handleChange} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="password">Senha</Label>
                        <Input type="password" name="password" id="password" value={this.state.password} onChange={this.handleChange} />
                    </FormGroup>
                    <div class="text-center">
                        {this.state.loading ? <Spinner color="warning" /> : ''}
                    </div>
                    <div class="text-center">
                        <Button color="success" type="submit">Enviar</Button>
                    </div>
                </Form>
            </div >
        )
    }
}
