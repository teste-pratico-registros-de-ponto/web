import React, { Component } from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import { Container, Row, Col } from 'reactstrap'

import Main from "./screens/Main.js"
import { ApolloProvider } from '@apollo/react-hooks';
import { client } from './utils/apollo'
import moment from 'moment'

export default class App extends Component {
  logout = () => {
    window.history.pushState('', 'Login', '/login')
    window.location.reload()
  }

  render() {
    return (
      <ApolloProvider client={client}>
        <Router>
          <Container>
            <Row>
              <div>
                <img src={require("./icons/logo.png")} alt="logo" style={{ width: "100px", display: 'absolute' }} />
              </div>
              <h3 style={{ marginLeft: '4vw' }}>Registro de Pontos</h3>
            </Row>
            <Row>
              <div style={{ float: 'right', marginTop: '3vh' }}>
                <img onClick={this.logout} src={require("./icons/logout.png")} alt="sair" style={{ width: '50px' }} />
              </div>
            </Row>
          </Container>

          <Switch>
            <Route path="/">
              <Main />
            </Route>
          </Switch>
          <Container fluid>
            <div style={{ width: '100%', backgroundColor: 'black', color: '#3d3d3d', marginTop: '20vh', textAlign: 'center' }}>
              <h6>@2020 Alexandre Frota Productions</h6>
            </div>
          </Container>
        </Router>
      </ApolloProvider>
    )
  }
}
