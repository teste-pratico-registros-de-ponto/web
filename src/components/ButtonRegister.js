import React from 'react';

import { ButtonEntry } from './ButtonEntry'
import { ButtonExit } from './ButtonExit'

export function ButtonRegister(props) {
    const lastHistory = props.lastHistory

    if (lastHistory) {
        const departure = lastHistory.departureTime
        if (departure) {
            return (
                <ButtonEntry />
            )
        } else {
            return (
                <ButtonExit historyId={props.lastHistory.id} />
            )
        }
    } else {
        return (
            <ButtonEntry />
        )
    }
}

