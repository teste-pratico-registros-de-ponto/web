import React, { Fragment } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

const GET_USER = gql`
    query User {
        user {
            name
        }
    }
`;

export function UserName(props) {
    const { loading, error, data } = useQuery(GET_USER);

    if (loading) return "Carregando..."
    if (error) return "Erro: " + error

    return (
        <Fragment>
            <div style={{ textAlign: 'center', marginTop: '2vh' }}>
                <h1>Painel do Colaborador</h1>
                <div style={{ marginTop: '5vh' }}>
                    <h2>{`${data.user.name}`}</h2>
                </div>

            </div>
        </Fragment>
    )
}

