import React, { Fragment } from 'react';
import { useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

import useForm from './useForm'

const ADD_USER = gql`
    mutation AddUser($name: String!, $mail: String!, $password: String!) {
        user(name: $name, mail: $mail, password: $password) {
            name
        }
    }
`;

export function AddUserScreen(props) {

    const [{ values, loading }, handleChange, handleSubmit] = useForm();

    const [addUser, { data }] = useMutation(ADD_USER);

    const novoUsuario = async () => {
        const histories = await addUser({
            variables: {
                name: values.name,
                mail: values.mail,
                password: values.password
            }
        })
        document.getElementById("form").reset();
    };

    return (
        <div>
            <h3>Registrar novo usuário</h3>
            <form onSubmit={handleSubmit(novoUsuario)} id="form">
                <input
                    onChange={handleChange}
                    type="text"
                    name="name"
                    placeholder="Digite o nome"
                />
                <input
                    onChange={handleChange}
                    type="text"
                    name="mail"
                    placeholder="Digite o e-mail"
                />
                <input
                    onChange={handleChange}
                    type="password"
                    name="password"
                    placeholder="Digite a senha"
                />
                <button type="submit">{loading ? "Enviando..." : "Enviar"}</button>
            </form>
        </div>
    )

}

