import React from 'react';
import { useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { Button } from 'reactstrap'

const DEPARTURE_TIME = gql`
    mutation Departure($id: Int) {
        departureTime(id: $id) {
            entryTime
        }
    }
`;

export function ButtonExit(props) {

    const [departureTime, { data }] = useMutation(DEPARTURE_TIME);

    async function sendDepartureTime() {
        console.log("Enviado")
        const histories = await departureTime({ variables: { id: props.historyId } })
        window.location.reload()
    }

    return (
        <Button color="danger" onClick={sendDepartureTime}>Saída</Button>
    )
}

