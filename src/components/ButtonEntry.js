import React, { Fragment } from 'react';
import { useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { Button } from 'reactstrap'

const SET_TIME = gql`
    mutation Register {
        historyTimeUser {
            entryTime
        }
    }
`;

export function ButtonEntry(props) {

    const [historyTimeUser, { data }] = useMutation(SET_TIME);

    async function newReg() {
        console.log("Enviado")
        const histories = await historyTimeUser()
        window.location.reload()
    }

    return (
        <Button color="primary" onClick={newReg}>Entrada</Button>
    )

}

