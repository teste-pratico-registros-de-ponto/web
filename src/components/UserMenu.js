import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { Container, Col } from 'reactstrap'

import { UserName } from './userName'
import { UserHistory } from './userHistory'
import { ButtonRegister } from './ButtonRegister'

const GET_HISTORY = gql`
    query History {
        historyTimeUser {
            id
            entryTime
            departureTime
            user {
                name
            }
        }
    }
`;

export function UserMenu(props) {

    const { data: histories, error: historiesError, loading: historiesLoading } = useQuery(GET_HISTORY);
    if (historiesLoading) return "Carregando..."
    if (historiesError) return "Erro: " + historiesError

    let lastHistory
    histories ? lastHistory = histories.historyTimeUser[0] : lastHistory = false

    console.log(lastHistory)

    return (
        <Container>
            <Col>
                <div><UserName /></div>
                <ButtonRegister lastHistory={lastHistory} />
                <UserHistory historyTimeUser={histories.historyTimeUser} />
            </Col>
        </Container>
    )
}

