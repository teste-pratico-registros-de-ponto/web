import React, { useEffect } from 'react';
import { Table, Container, Row, Col } from 'reactstrap';

import moment from 'moment'

const momentDate = (dateTime, format) => {
    return moment.unix(dateTime / 1000).format(format)
}

const dateFormat = (dateTime, format) => {
    if (!dateTime) return "..."
    return momentDate(dateTime, format)
}

export function UserHistory(props) {
    return (
        <Row>
            <Col sm={12} className="mt-4">
                {props.admin ? <h3>Histórico dos colaboradores</h3> : <h3>Seu histórico</h3>}
            </Col>
            <Col sm={12}>
                <Col className="mt-4">
                    <Table striped bordered hover size="sm">
                        <thead>
                            <tr>
                                {props.admin ? <th>Nome</th> : ''}
                                <th>Entrada</th>
                                <th>Hora</th>
                                <th>Saída</th>
                                <th>Hora</th>
                            </tr>
                        </thead>
                        <tbody>
                            {props.historyTimeUser.map((history) => (
                                <tr key={history.id}>
                                    {props.admin ? <td>{history.user.name}</td> : ''}
                                    <td>{dateFormat(history.entryTime, "MMM DD dddd")}</td>
                                    <td>{dateFormat(history.entryTime, "hh:mm")}</td>
                                    <td>{dateFormat(history.departureTime, "MMM DD dddd")}</td>
                                    <td>{dateFormat(history.departureTime, "hh:mm")}</td>
                                </tr>
                            ))}
                        </tbody>
                    </Table >
                </Col>
            </Col>
        </Row>
    )
}

