import React, { useState } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { Button } from 'reactstrap'

import { UserName } from './userName'
import { UserHistory } from '../components/userHistory'
import { AddUserScreen } from './AddUserScreen'

const GET_HISTORY = gql`
    query History {
        adminViewHistories {
            id
            entryTime
            departureTime
            user {
                name
            }
        }
    }
`;

export function AdminMenu(props) {
    const { data, error, loading } = useQuery(GET_HISTORY);
    const [userForm, setUserForm] = useState(false)

    if (loading) return "Carregando..."
    if (error) return "Erro: " + error

    const userHistories = data.adminViewHistories

    const turnAddUserForm = () => {
        let newUserForm = !userForm
        newUserForm = setUserForm(newUserForm)
    }

    return (
        <div>
            <h3>Tela do Administrador</h3>
            <UserName />
            <Button color="success" onClick={turnAddUserForm}>{userForm ? 'Esconder' : 'Adicionar Usuário'}</Button>
            {userForm ? <AddUserScreen /> : ''}
            <UserHistory historyTimeUser={userHistories} admin={true} />
        </div>
    )
}

