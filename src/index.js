import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { BrowserRouter as Router, Route } from "react-router-dom";
import history from "./utils/history";
import 'bootstrap/dist/css/bootstrap.min.css';

import Login from "./screens/Login";

ReactDOM.render(
  <Router history={history}>
    <Route path='/' exact component={App} />
    <Route path='/login' exact component={Login} />
  </Router >
  , document.getElementById('root'));