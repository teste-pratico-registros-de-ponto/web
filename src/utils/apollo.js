import ApolloClient from 'apollo-boost';

export const client = new ApolloClient({
    uri: 'http://localhost:3200/main',
    headers: {
        authorization: `Bearer ${localStorage.getItem('token')}`
    }
});